#!/bin/bash

FONTS_DIRECTORY="/home/${USER}/fonts-loader"
DOWNLOADS_FOLDER="/home/${USER}/Downloads/"

files_to_find=`find /home/${USER}/Downloads/ -name *.ttf -o -name *.xz -o -name \*.zip`

# A little title for our script
echo -e "\n\e[32m\e[1m            -== Fonts Installation Script ==-\n\n"
echo -e "\e[34mChecking if receptacle folder exists..."
sleep 1

#Cheking of directory fonts-loader exists
if [[ -d "${FONTS_DIRECTORY}" ]]; then
	echo -e "\e[39m=> ${FONTS_DIRECTORY} exists on your filesystem."
else
	echo -e "\e[32m${FONTS_DIRECTORY} doesn't exist, do you want to create it ? [Y/y || N/n]"
	read createAnswer

	if [[ ${createAnswer} = "Y" || ${createAnswer} = "y" ]]; then
		echo -e "\e[39mCreating folder in \e[1m/home"
		mkdir ${FONTS_DIRECTORY}
		sleep 1
		echo "Done"
	elif [[ ${createAnswer} = "N" || ${createAnswer} = "n" ]]; then
		echo -e "\e[31mThis folder needs to exist... \e[39mCreating it anyway ;)"
		mkdir ${FONTS_DIRECTORY}
		sleep 1
		echo "Done"
	else
		echo -e "\e[31m =>Error, exiting program..."
		exit
	fi
fi

#cd'ing in downloads before extracting zip and tar files into folder
cd /home/enzo/Downloads

# Loops for every extentions in order to un pack zip AND tar + moving eventual alone fonts files already in downloads folder
echo -e "\n\n\e[39m=> Findind eventual ZIP / TAR files needing to be unziped..."
# find . -iname \*.zip -o -iname \*.xz

for files in ${files_to_find}; do
	if [[ ${#files} -gt 0 ]]; then
		echo -e "\e[1m\e[32mFound: \e[0m\e[32m${files##*/}"
		echo -e "\e[0m\e[39mExtracting content to ${FONTS_DIRECTORY}"
		if [[ ${file##*/.} == "zip" ]]; then
			unzip ${files##*/} -d ${FONTS_DIRECTORY}
		elif [[ ${file##*/.} = ="xz" ]]; then
			tar -xf ${files##*/} -C ${FONTS_DIRECTORY}
		else
			echo -e "\e[1m\e[31mError..."
		fi
		echo ""
	else
		echo -e "\e[31m Zero accepted files were found..."
	fi
done

#Moving our ttf files inside /.fonts
#echo -e "\e[91mMoving fonts inside :\e[1m /home/${USER}/.fonts\e[39m"
#for toMove in $ttf_files; do
#	mv $toMove /home/${USER}/.fonts
#done

# Rebuild font cache
#sudo fc-cache -f -v

echo -e "\n\e[32m\e[1mDONE !"
